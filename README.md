# Magellan BETA Testing Program

Please post all bugs here.  You will be able to track and communicate with the development team via this repository. 
IT IS imperative that you check back frequently for questions from the development team. 

When reporting issues, please incude the following format:


*  Class / Level / Professions / Zone Your Toon is in 
*  Brief comment on what the error is
*  What was enabled on Magellan
*  If you were using a saved route, attach the route

If this was a WOW error, paste JUST WHAT SHOWS UP AT THE TOP OF THE ERROR WINDOW. NOT THE ENTIRE ERROR PLEASE.



